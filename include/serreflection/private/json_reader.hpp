#ifndef SERREFLECTION_JSON_READER_H
#define SERREFLECTION_JSON_READER_H

#include <serreflection/private/base_reader.hpp>

namespace srfl
{
template <typename T, typename Tpt>
void read_json_p(Tpt&& pt, T& object);

namespace
{
bool check_default(const double val)
{
    return std::isnan(val);
}

bool check_default(const std::string& val)
{
    if ("NaN" == val)
    {
        return true;
    }
    return false;
}
}

template <typename T>
class JSONReader : public BaseReader<T, boost::property_tree::ptree, JSONReader<T>>
{
  public:
    template <typename Tpt>
    JSONReader(Tpt&& pt)
        : BaseReader<T, boost::property_tree::ptree, JSONReader<T>>(std::forward<Tpt>(pt))
    {
    }

    template <typename Tpt>
    JSONReader(Tpt&& pt, T& object)
        : BaseReader<T, boost::property_tree::ptree, JSONReader<T>>(std::forward<Tpt>(pt), object)
    {
    }

    template <typename Targ>
    bool read_optional(Targ& element, const std::string& name,
                       const bool is_fundamental = true) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        try
        {
            if (is_fundamental)
            {
                element = pt.get<Targ>(name);
            }
            else
            {
                pt.get_child(name);
            }
            return true;
        }
        catch (const std::exception& ex)
        {
            auto msg = "Failed to read variable: " + srfl::StructChecker<T>::class_name +
                       "::" + name + " : " + ex.what();

            auto default_val = srfl::StructChecker<T>::get_default_value(name, element);

            if (check_default(default_val))
            {
                throw std::runtime_error(msg);
            }

            BL_WARNING() << msg << ". Use default value: " << default_val;

            element = default_val;
            return false;
        }
    }

    template <typename Tvector>
    void read_fundamental_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        Tvector default_val;

        if (!read_optional(default_val, name, false))
        {
            element.resize(1);
            element[0] = default_val;
            return;
        }

        for (auto& item : pt.get_child(name))
        {
            boost::property_tree::ptree tmp = item.second;
            element.push_back(tmp.get<Tvector>(""));
        }
    }

    template <typename Tvector>
    void read_objects_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        for (auto& item : pt.get_child(name))
        {
            boost::property_tree::ptree tmp = item.second;
            element.emplace_back();
            auto& rf = element.back();
            read_json_p(std::move(tmp), rf);
        }
    }

    void read_string(std::string& element, const std::string& name) const
    {
        read_optional(element, name);
    }

    template <typename Targ>
    void read_fundamental(Targ& element, const std::string& name) const
    {
        read_optional(element, name);
    }

    template <typename Targ>
    void read_enum(Targ& element, const std::string& name) const
    {
        std::string value;

        if (!read_optional(value, name))
        {
            return;
        }

        // auto default_val = srfl::StructChecker<T>::get_default_value(name, element);

        auto it = srfl::EnumParser<Targ>::parse_map.find(value);

        if (srfl::EnumParser<Targ>::parse_map.end() == it)
        {
            auto msg = "Failed to read variable: " + srfl::StructChecker<T>::class_name +
                       "::" + name + " : unexpected value \"" + value + "\"";

            throw std::runtime_error(msg);
        }
        else
        {
            element = it->second;
        }
    }

    template <typename Targ>
    void read_class(Targ& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;
        read_json_p<Targ>(std::move(pt.get_child(name)), element);
    }
};
}

#endif
